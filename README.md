1. **Dog app**
- Working with API using Retrofit, Gson, rxjava ([https://raw.githubusercontent.com/…/DogsApi/master/dogs.json](https://raw.githubusercontent.com/…/DogsApi/master/dogs.json))

- Use Jetpack navigation graph to design list and details

- Organize code in attached package: view, viewmodel, model

- Implement Data binding

2. **Screenshot**

![alt text](https://i.imgur.com/baGkYdT.png) ![alt text](https://i.imgur.com/lXmD3L8.png) ![alt text](https://i.imgur.com/R92u2k8.png) 