package com.hoanphan.dogapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoanphan.dogapp.model.DogBreed;
import com.squareup.picasso.Picasso;

import retrofit2.http.Tag;

import static android.content.ContentValues.TAG;

public class DetailFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
//        String message = args.getMessage();
//        Log.i(TAG,"onViewCreated: "+ message);
//        DogBreed dogBreed = args.getDog();
//        Log.i(TAG,"onViewCreated: "+ dogBreed.toString());

        TextView namedog = getView().findViewById(R.id.name);
        TextView tvOrigin = getView().findViewById(R.id.origin);
        TextView tvLifeSpan = getView().findViewById(R.id.life_span);
        ImageView imgDog = getView().findViewById(R.id.image);
        TextView TemDog = getView().findViewById(R.id.tv_template);

        namedog.setText(args.getDog().name);
        tvLifeSpan.setText(args.getDog().lifeSpan);
        tvOrigin.setText(args.getDog().origin);
        TemDog.setText(args.getDog().temperament);
        Picasso.with(getContext()).load(args.getDog().url).into(imgDog);


       //tvLifepan.setText(dogBreed.lifeSpan);
           // Picasso.with(DetailFragment.class).load(dogBreed.url).into(holder.imgAvatar);
    }
    }
}