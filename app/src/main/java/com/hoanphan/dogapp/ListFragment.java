package com.hoanphan.dogapp;

import android.content.ClipData;
import android.graphics.Canvas;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ItemTouchHelper.Callback;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanphan.dogapp.Adapter.DogAdapter;
import com.hoanphan.dogapp.model.DogBreed;
import com.hoanphan.dogapp.modelview.DogApiService;
import com.hoanphan.dogapp.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class ListFragment extends Fragment {
    RecyclerView recyclerViewDogs;
    DogAdapter dogAdapter;
    private DogApiService apiService;
    private SwipeRefreshLayout swipeRefreshLayout;
    View view;
    ArrayList<DogBreed> listDog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerViewDogs = view.findViewById(R.id.rvDogs);
        listDog = new ArrayList();
        dogAdapter = new DogAdapter(getContext(), listDog);

        setHasOptionsMenu(true);
        apiService = new DogApiService();

        recyclerViewDogs.setLayoutManager(new GridLayoutManager(getContext(),2));
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               swipeRefreshLayout.setRefreshing(false);
            }
        });
        getData();
        return view;
    }

    public void getData() {
        apiService.getAllDogs()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<DogBreed>>() {
                    @Override
                    public void onSuccess(@NonNull List<DogBreed> dogBreeds) {
                        for(DogBreed dog: dogBreeds) {
                            Log.d("DEBUG1", dog.name);
                            listDog.add(dog);
                        }
                        dogAdapter = new DogAdapter(getContext(), listDog);
                        recyclerViewDogs.setAdapter(dogAdapter);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_dog, menu);
        MenuItem mSearch = menu.findItem(R.id.appSearchBar);
        SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search something ");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                 dogAdapter.getFilter().filter(newText);
                return true;
            }

        });
    }

}