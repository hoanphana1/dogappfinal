package com.hoanphan.dogapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.hoanphan.dogapp.DetailFragment;
import com.hoanphan.dogapp.ListFragment;
import com.hoanphan.dogapp.ListFragmentDirections;
import com.hoanphan.dogapp.R;
import com.hoanphan.dogapp.model.DogBreed;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DogAdapter extends RecyclerView.Adapter<DogAdapter.ViewHolder> {
    Context context;
    ArrayList<DogBreed> listdogs;
//    private ArrayList<DogBreed> listdogs1;
//    private ArrayList<DogBreed> listdogs2;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();


    public DogAdapter(Context context, ArrayList<DogBreed> listdogs) {
        this.context = context;
        this.listdogs = listdogs;
//        this.listdogs1 = new ArrayList<DogBreed>(listdogs);
//        this.listdogs2 = new ArrayList<DogBreed>(listdogs);
//        listdogs1 = listdogs;
    }

    @NonNull
    @Override
    public DogAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dog_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DogAdapter.ViewHolder holder, int i) {
        viewBinderHelper.setOpenOnlyOne(true);
        DogBreed dogBreed = listdogs.get(i);
        holder.txtTenCho.setText(dogBreed.name);
        holder.txtDecriptions.setText(dogBreed.lifeSpan);
        holder.tvInfo.setText("Lifespan: "+dogBreed.lifeSpan+"\n Origin: " +
                dogBreed.origin + "\n Temperament: "+ dogBreed.temperament);
        Picasso.with(context).load(dogBreed.url).placeholder(R.drawable.progress_animation).into(holder.imgDog);
    }

    @Override
    public int getItemCount() {
        return listdogs.size();
    }
    public Filter getFilter() {
        return exampleFilter;
    }
    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<DogBreed> filteredList = new ArrayList<DogBreed>();
            Log.d("DEBUG4", String.valueOf((ArrayList) listdogs));
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listdogs);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (DogBreed item : listdogs) {
                    if (item.name.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            Log.d("DEBUG20",String.valueOf((List) results.values));
            Log.d("DEBUG22", String.valueOf((List) listdogs));
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            Log.d("DEBUG21", (String) constraint);
            listdogs = (ArrayList<DogBreed>) results.values;
            // mContacts.addAll((List) results.values);
            listdogs = listdogs;
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtTenCho,txtDecriptions,tvInfo;
        ImageView imgDog;
        LinearLayout cardDog;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvInfo = itemView.findViewById(R.id.tv_info);

            cardDog = itemView.findViewById(R.id.card_dog);
            txtTenCho = itemView.findViewById(R.id.txtName);
            txtDecriptions = itemView.findViewById(R.id.txtDecription);
            imgDog = itemView.findViewById(R.id.imgAvatar);
            cardDog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final NavController navController = Navigation.findNavController(view);
//                    navController.navigate(R.id.action_listFragment_to_detailFragment);
                    int position = getAdapterPosition();
                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(listdogs.get(position));
                    action.setMessage("this is the message...");
                    navController.navigate(action);
                }
            });
        }
    }

}
