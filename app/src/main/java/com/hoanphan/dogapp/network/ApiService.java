package com.hoanphan.dogapp.network;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static ApiService instance;

    private static final HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    private static final OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

    private DogApi api;

    public static ApiService getInstance() {
        if (instance == null) instance = new ApiService();
        return instance;
    }

    private ApiService(){
    api = new Retrofit.Builder()
            .baseUrl(NetworkConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(client)
                .build()
                .create(DogApi.class);
}

    public DogApi getAPI() {
        return api;
    }
}
