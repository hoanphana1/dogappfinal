package com.hoanphan.dogapp.modelview;

import com.hoanphan.dogapp.model.DogBreed;
import com.hoanphan.dogapp.network.DogApi;

import java.util.List;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import io.reactivex.rxjava3.core.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class
DogApiService {

    private final String BASE_URL="https://raw.githubusercontent.com";
    private DogApi api;

    private static final HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    private static final OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

    public DogApiService(){
        api = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(client)
                .build()
                .create(DogApi.class);
    }
    public Single<List<DogBreed>> getAllDogs(){
        return api.getAllDogs();
    }
}
