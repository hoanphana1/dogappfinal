package com.hoanphan.dogapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DogBreed implements Parcelable {
    @SerializedName("id")
    public final int id;

    @SerializedName("name")
    public final String name;

    @SerializedName("origin")
    public final String origin;

    @SerializedName("life_span")
    public final String lifeSpan;

    @SerializedName("url")
    public final String url;

    @SerializedName("temperament")
    public final String temperament;

    public DogBreed(int id, String name, String origin, String lifeSpan, String url, String temperament) {
        this.id = id;
        this.name = name;
        this.origin = origin;
        this.lifeSpan = lifeSpan;
        this.url = url;
        this.temperament = temperament;
    }

    protected DogBreed(Parcel in) {
        id = in.readInt();
        name = in.readString();
        origin = in.readString();
        lifeSpan = in.readString();
        url = in.readString();
        temperament = in.readString();
    }

    public static final Creator<DogBreed> CREATOR = new Creator<DogBreed>() {
        @Override
        public DogBreed createFromParcel(Parcel in) {
            return new DogBreed(in);
        }

        @Override
        public DogBreed[] newArray(int size) {
            return new DogBreed[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(origin);
        parcel.writeString(lifeSpan);
        parcel.writeString(url);
    }

    public boolean isShowMenu() {
        return true;
    }
}
